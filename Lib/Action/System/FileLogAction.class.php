<?php
class FileLogAction extends BackAction{
	public function index(){
		C('TOKEN_ON',false);
		if($_GET['title'] != ""){
			$where = array();
			$where['msg'] = array('like',"%".$_GET['title']."%");
		}
		$count = M('update_record')->where($where)->count();
		$page = new Page($count,25);
		$log = M('update_record')->where($where)->order('time desc')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('log',$log);
		$this->assign('page',$page->show());
		$this->display();
	}
}