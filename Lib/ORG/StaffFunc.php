<?php
	$data = [
		'Function' => [
			'funname' => 'Function',
			'name' => '功能展示'
		],
		'Areply' => [
			'funname' => 'Areply',
			'name' => '关注时回复与帮助'
		],
		'Text' => [
			'funname' => 'Text',
			'name' => '文本回复'
		],
		'Picreply' => [
			'funname' => 'Picreply',
			'name' => '图片回复'
		],
		'Img' => [
			'funname' => 'Img',
			'name' => '图文回复'
		],
		'Voiceresponse' => [
			'funname' => 'Voiceresponse',
			'name' => '语音回复'
		],
		'Message' => [
			'funname' => 'Groupmessage',
			'name' => '群发消息'
		],
		'TemplateMsg' => [
			'funname' => 'TemplateMsg',
			'name' => '模板消息'
		],
		'SendTemplateMsg' => [
			'funname' => 'SendTemplateMsg',
			'name' => '微信模板消息群发'
		],
		'Company' => [
			'funname' => 'Company',
			'name' => 'LBS商家连锁'
		],
		'Conditionalmenu' => [
			'funname' => 'Conditionalmenu',
			'name' => '个性化自定义菜单'
		],
		'Auth' => [
			'funname' => 'Auth',
			'name' => '自动获取粉丝信息'
		],
		'Other' => [
			'funname' => 'Other',
			'name' => '回答不上来的配置'
		],
		'Quick' => [
			'funname' => 'Quick',
			'name' => '快捷关注配置'
		],
		'CommentAndReply' => [
			'funname' => 'CommentAndReply',
			'name' => '评论回复管理'
		],
		'Recognition' => [
			'funname' => 'Recognition',
			'name' => '渠道二维码'
		],
	];
	$i = 100000;
	foreach($data as $key=>$val){
		if(!isset($val['id'])){
			$data[$key]['id'] = $i;
			$i++;
		}
	}
	return $data;
?>
