<?php

/**
 * Created by PhpStorm.
 * User: Ann
 * Date: 12/28/2016
 * Time: 15:41
 */
class AuditingModel extends Model
{
    private static $modules_convert = array(
        'Index' => 'Img',
        'Autumns' => 'Activity',
        'Photo' => 'Reply_info',
        'Repast' => 'Reply_info',
        'Store' => 'Reply_info',
        'Hotels' => 'Reply_info',
        'Groupon' => 'Reply_info',
        'Panorama' => 'Reply_info',
        'Forum' => 'Reply_info',
        'Medical' => 'medicalSet',
        'School' => 'Schoolset',
        'Red_packet' => 'Packet',
        'Guajiang' => 'Lottery',
        'Coupon' => 'Lottery',
        'LuckyFruit' => 'Lottery',
        'GoldenEgg' => 'Lottery',
        'AppleGame' => 'Lottery',
        'Lovers' => 'Lottery',
        'Autumn' => 'Lottery',
        'Jiugong' => 'Lottery',
    );
    /**
     * 设置活动审核
     * @param $token
     * @param $module
     * @param $id string
     */
    public function setAuditing($token, $module, $id, $keyword)
    {   
        $ignoreKeyword = array('waphelp', 'Text');
        if (in_array($keyword, $ignoreKeyword)) {
            return;
        }

        $auditingCfg = include LIB_PATH . 'ORG/auditing.config.php';
        if(!array_key_exists($module, $auditingCfg) ) {
            return;
        }

        $config = $auditingCfg[$module];

        // 扩展处理
        if(isset($config['extend']) ) {
            $extend = call_user_func($config['extend'], $token, $id, $keyword);
            if($extend !== false && is_array($extend)) {
                $config = array_merge($config, $extend);
            } else {
                return;
            }
        }

        // 活动标题
        if(!isset($config['title']) ) {
            $title = '';
        } elseif (is_array($config['title']) ) {
            $cfgTitle = $config['title'];
            $where = isset($cfgTitle['where']) ? $cfgTitle['where'] : 'id=ID';
            $where = str_replace(array('TOKEN', 'ID'), array($token, $id), $where);
            $titleField = isset($cfgTitle['field']) ? $cfgTitle['field'] : 'title';
            $md = M($cfgTitle['tb']);
            $title = $md->where($where)->getField($titleField);
        } elseif (is_string($config['title']) ) {
            $title = $config['title'];
        }

        // 关键URL
        $config['url'] = isset($config['url']) ? str_replace(array('ID'), array($id), $config['url']) : "";
        $urlArr = $this->_query2array($config['url']);
        $config['param'] = isset($config['param']) ? str_replace(array('ID'), array($id), $config['param']) : '';
        $paramArr = $this->_query2array($config['param']);

        // 默认值处理
        if(!isset($urlArr['a']) && !isset($paramArr['a']) ) {
            $paramArr['a'] = 'index';
        }
        if(!isset($urlArr['m']) ) {
            $urlArr['m'] = isset($paramArr['m']) ? $paramArr['m'] : $module;
        }
        unset($paramArr['m']);

        // 计算指纹
        $urlArr['token'] = $token;
        $ID = $this->_fingerprint(array_merge($urlArr, $paramArr) );
        $FP = $this->_fingerprint($urlArr);
        unset($urlArr['token']);

        // 更新数据
        $data = array(
            'title' => isset($title) ? $title : '',
            'keyword' => $keyword,
            'param' => serialize($paramArr),
            'url' => serialize($urlArr),
            'fp' => $FP,
            'is_edit' => '1',
            'update_time' => NOW_TIME
        );

        // 保存或更新审核记录
        if($this->where(array('id' => $ID) )->count() ) {
            $this->where(array('id' => $ID) )->save($data);
        } else {
            $data['id'] = $ID;
            $data['allowed'] = 0;
            $data['token'] = $token;
            $data['module'] = $module;
            $data['create_time'] = NOW_TIME;

            //分配任务到审核员
            $auditor_list = S('AUDITING_AUDITOR');
            if(!$auditor_list) {
                $response = HttpClient::getInstance()->get('http://booking.meihua.com/?s=auditing/index/auditor.json');
                if($response && ($auditor_list = json_decode($response, 1))) {
                    S('AUDITING_AUDITOR', $auditor_list, 86400);
                } else {
                    $auditor_list = [];
                }
            }

            $auditorIdx = S('AUDITING_AUDITOR_IDX');
            $auditorIdx || ($auditorIdx = 0);
            if($auditorIdx >= count($auditor_list)) {
                $auditorIdx = 0;
            }

            $data['auditor'] = $auditor_list[$auditorIdx];
            S('AUDITING_AUDITOR_IDX', $auditorIdx + 1);

            $this->add($data);
        }
    }

    /**
     * 检查当前页面审核状态
     * @param $token
     * @return bool
     */
    public function isForbid($token)
    {
        $module = isset(self::$modules_convert[MODULE_NAME]) ? self::$modules_convert[MODULE_NAME] : MODULE_NAME;
        // 取指纹变量
        $auditingCfg = include LIB_PATH . 'ORG/auditing.config.php';
        if(!array_key_exists($module, $auditingCfg) ) {
            return false;
        }

        $config = $auditingCfg[$module];

        if(isset($config['extend']) ) {
            $extend = call_user_func($config['extend'], $token);
            if($extend && is_array($extend) ) {
                $config = array_merge($config, $extend);
            }
        }

        $fp = $this->_query2array(isset($config['url']) ? $config['url'] : []);
        /*if(MODULE_NAME == 'AppleGame') {
            dump($fp);
            exit;
        }*/

        // 从当前URL取指纹值
        foreach ($fp as $k => $v) {
            if(isset($_GET[$k])) {
                $fp[$k] = $_GET[$k];
            }
        }

        $fp['m'] = MODULE_NAME;
        $fp['token'] = $token;
        $fpValue = $this->_fingerprint($fp);

        // 对比是否审核拒绝
        return $this->where(array('fp' => $fpValue, 'allowed' => '-1') )->count() > 0;
    }

    private function _query2array($query)
    {
        if(is_array($query) ) {
            return $query;
        }

        $items = explode('&', trim($query, '&') );
        $param = array();
        foreach ($items as $vo) {
            if(strpos($vo, '=')) {
                $split = explode('=', $vo);
                $param[$split[0] ] = $split[1];
            }
        }

        return $param;
    }

    /**
     * 计算指纹
     * @param $data array
     * @return string
     */
    private function _fingerprint($data)
    {
        ksort($data);
        return md5(json_encode($data) );
    }

    /**
     * 统一审核平台获取可审核token列表
     */
    public function remoteAuth($authKey)
    {
        $auth = S('AUDIT_' . $authKey);
        if(!$auth) {
            $response = HttpClient::getInstance()->get('http://booking.meihua.com/?s=auditing/index/authorize.json&key=' . $authKey);
            if($response && ($responseJson = json_decode($response, 1)) && $responseJson['status']) {
                $auth = $responseJson['info'];
                S('AUDIT_' . $authKey, $auth, 3600);
            }
        }

        return $auth;
    }


    static function lottery($token, $id) {
        if(!$id) {
            return array();
        }

        $lt = array('Lottery', 'Guajiang', 'Coupon', 'LuckyFruit', 'GoldenEgg', '', 'AppleGame', 'Lovers', 'Autumn', 'Jiugong');
        $lottery = M('Lottery')->where(array('id' => $id) )->field('title,type')->find();
        $module = $lt[$lottery['type'] - 1];

        return array(
            'title' => $lottery['title'],
            'url' => "m={$module}",
            'param' => "a=index&id=$id&type={$lottery['type']}"
        );
    }

    static function business($token, $id) {
        if($id) {
            $item = M('Busines')->where(array('bid' => $id))->find();

            return array(
                'title' => $item['title'],
                'param' => "a=index&bid=$id&type={$item['type']}"
            );
        }

        return [];
    }

    static function reply_info($token, $id, $keyword) {
        if($id) {
            $lt = array(
                '相册' => 'm=Photo&a=index',
                '订餐' => 'm=Repast&a=index',
                '商城' => 'm=Store&a=index',
                '酒店' => 'm=Hotels&a=index',
                '团购' => 'm=Groupon&a=grouponIndex',
                '全景' => 'm=Panorama&a=index',
                '论坛' => 'm=Forum&a=index',
                '讨论社区' => 'm=Forum&a=index',
            );

            if(!isset($lt[$keyword])) {
                return false;
            }

            return array(
                'title' => $keyword,
                'param' => $lt[$keyword],
            );
        } else {
            return array();
        }
    }
}